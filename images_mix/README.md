# Images mix - how to use

1. First, you have to special folders for images.
2. In folder ./test you have to have folders for chess pieces images not mixed:

- ./bk - black king
- ./bq - black queen
- ./bb - black bishop
- ./br - black rook
- ./bn - black knight
- ./bp - black pawn
- ./em - empty
- ./wk - white king
- ./wq - white queen
- ./wb - white bishop
- ./wr - white rook
- ./wn - white knight
- ./wp - white pawn

3. In folder ./train you have to have folders for chess pieces images not mixed:

- ./bk - black king
- ./bq - black queen
- ./bb - black bishop
- ./br - black rook
- ./bn - black knight
- ./bp - black pawn
- ./em - empty
- ./wk - white king
- ./wq - white queen
- ./wb - white bishop
- ./wr - white rook
- ./wn - white knight
- ./wp - white pawn

4. Put your test and train images into relevant folders.
5. Create folder ./mix, where will be all mixed images in relevant folders:

- ./bk - black king
- ./bq - black queen
- ./bb - black bishop
- ./br - black rook
- ./bn - black knight
- ./bp - black pawn
- ./em - empty
- ./wk - white king
- ./wq - white queen
- ./wb - white bishop
- ./wr - white rook
- ./wn - white knight
- ./wp - white pawn

6. Create folder ./train_mix with relevant folders, where will be 80% of all images:

- ./bk - black king
- ./bq - black queen
- ./bb - black bishop
- ./br - black rook
- ./bn - black knight
- ./bp - black pawn
- ./em - empty
- ./wk - white king
- ./wq - white queen
- ./wb - white bishop
- ./wr - white rook
- ./wn - white knight
- ./wp - white pawn

7. Create folder ./test_mix with relevant folders, where will be 20% of all images:

- ./bk - black king
- ./bq - black queen
- ./bb - black bishop
- ./br - black rook
- ./bn - black knight
- ./bp - black pawn
- ./em - empty
- ./wk - white king
- ./wq - white queen
- ./wb - white bishop
- ./wr - white rook
- ./wn - white knight
- ./wp - white pawn

8. In the script, mix_photo.py set a piece of chess, which you want to mix in path folders. For example:

```python
'./test/wr'
```

```python
'./train/wr'
```

```python
'./mix/wr'
```

```python
'./train_mix/wr'
```

9. Run script:

```bash
python3 mix_photo.py
```
