import os
import random
import shutil


test_photo = []
test_photo = os.listdir('./test/wr')

train_photo = []
train_photo = os.listdir('./train/wr')

out_folder_path = './mix/wr'
in_folder_path = './test/wr'
for image_name in test_photo:
    cur_image_path = os.path.join(in_folder_path, image_name)
    cur_image_out_path = os.path.join(out_folder_path, image_name)
    shutil.copyfile(cur_image_path, cur_image_out_path)


out_folder_path = './mix/wr'
in_folder_path = './train/wr'
for image_name in train_photo:
    cur_image_path = os.path.join(in_folder_path, image_name)
    cur_image_out_path = os.path.join(out_folder_path, image_name)
    shutil.copyfile(cur_image_path, cur_image_out_path)


photos = test_photo + train_photo

random.shuffle(photos)

test_photo_new = photos[int(len(photos) * .0) : int(len(photos) * .2)]
train_photo_new = photos[int(len(photos) * .2) : int(len(photos) * 1)]



out_folder_path = './train_mix/wr'
in_folder_path = './mix/wr'
for image_name in train_photo_new:
    cur_image_path = os.path.join(in_folder_path, image_name)
    cur_image_out_path = os.path.join(out_folder_path, image_name)
    shutil.copyfile(cur_image_path, cur_image_out_path)


out_folder_path = './test_mix/wr'
in_folder_path = './mix/wr'
for image_name in test_photo_new:
    cur_image_path = os.path.join(in_folder_path, image_name)
    cur_image_out_path = os.path.join(out_folder_path, image_name)
    shutil.copyfile(cur_image_path, cur_image_out_path)

