# Image cutter - how to use
1. Create directory containing all `.JPG` photos from chess game and `.pgn` file.
2. By default work directories are `./images/to_process/` and `./images/processed/`.
3. Make sure your chessboard images are made correctly (there aren't cut rows or columns etc.).
4. Add that directory to `./images/to_process/`.
5. Run: `python cut.py`
6. All processed images were cropped and divided into appropriate directories in `./images/processed/`.

## Images naming convention
It should make database debugging easier.
- Image with initial game state should be named `<game_id>_0.JPG`
- Next image names should contain information about game state `<game_id>_<move>_<turn>.JPG`

### Example:

|<img src="readme_images/1_0.JPG" width="320"/> |
|:--:|
| *1_0.JPG* |

|<img src="readme_images/1_1_1.JPG" width="320"/> |
|:--:|
| *1_1_1.JPG* |

|<img src="readme_images/1_1_2.JPG" width="320"/> |
|:--:|
| *1_1_2.JPG* |

|<img src="readme_images/1_2_1.JPG" width="320"/> |
|:--:|
| *1_2_1.JPG* |