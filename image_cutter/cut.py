import os
import cv2 as cv
import chess
import chess.pgn
from chess_dictionary import chess_dict
import re


TARGET_DIR = "./images/to_process"
RESULT_DIR = "./images/processed"


def crop_image(b_image):
    """ Crop one image to 64 pictures """
    board_images = []
    height, width, _ = b_image.shape
    field_size = (int(height / 8), int(width / 8))
    for i in range(8):
        for j in range(8):
            field = [i * field_size[0], j * field_size[1]]
            board_images.append(b_image[field[0]: field[0] + field_size[0],
                                field[1]: field[1] + field_size[1]])
    return board_images


def sorted_alphanumeric(data):
    """ Special images sorting for game_move_player.JPG naming convention """
    convert = lambda text: int(text) if text.isdigit() else text.lower()
    alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)]
    return sorted(data, key=alphanum_key)


def display_cropped_images(board_images):
    """ Display all cropped images """
    for index, image in enumerate(board_images):
        image = cv.resize(image, (150, 150))
        cv.namedWindow(f"output_{index}")
        cv.imshow(f"output_{index}", image)
    print("Press any key to close displayed images")
    cv.waitKey(0)
    cv.destroyAllWindows()


def get_all_processed_directories():
    """ Search for and return all directories containing games """
    directories = []
    for directory in sorted(os.listdir("{}".format(TARGET_DIR))):
        directories.append(directory)
    return directories


def save_cropped_images(board_images, pieces_list, filenames_from):
    """ Save all images from processed game """
    for index, image in enumerate(board_images):
        save_directory = chess_dict.get(str(pieces_list[index]))
        cv.imwrite("{}/{}/{}_{}.JPG".format(RESULT_DIR, save_directory,
                                            filenames_from[int(index/64)], index), image)


def load_virtual_board(processed_directory):
    """ Load virtual chessboard from chess.pgn library for specific game """
    for filename in os.listdir("{}/{}".format(TARGET_DIR, processed_directory)):
        if filename.endswith(".pgn"):
            with open("{}/{}/{}".format(TARGET_DIR, processed_directory, filename)) as pgn:
                return chess.pgn.read_game(pgn)
    raise Exception(".pgn file not found in processed folder")


def load_images_to_process(directory):
    """ Load all .JPG images from particular game to be processed """
    board_images, filenames = [], []
    filenames_to_process = os.listdir("{}/{}".format(TARGET_DIR, directory))
    filenames_to_process = sorted_alphanumeric(filenames_to_process)
    for filename in filenames_to_process:
        if filename.endswith(".JPG") or filename.endswith(".jpg"):
            board_images.append(cv.imread("{}/{}/{}".format(TARGET_DIR, directory, filename)))
            filenames.append(filename[:len(filename) - 4])
    return filenames, board_images


def create_dirs_for_processed_images():
    """ Create directories for chess pieces - names set in chess_dictionary.py """
    if not os.path.exists("{}".format(RESULT_DIR)):
        os.makedirs("{}".format(RESULT_DIR))
    for value in chess_dict.values():
        if not os.path.exists("{}/{}".format(RESULT_DIR, value)):
            os.makedirs("{}/{}".format(RESULT_DIR, value))


def read_game_pieces_list(board, game):
    """ Create list of pieces from game in correct order """
    pieces_list = []
    for index, move in enumerate(game.mainline_moves()):
        for i in range(8, 0, -1):
            for j in range(i * 8 - 8, i * 8, 1):
                pieces_list.append(board.piece_at(j))
        board.push(move)
    for i in range(8, 0, -1):
        for j in range(i * 8 - 8, i * 8, 1):
            pieces_list.append(board.piece_at(j))
    return pieces_list


def create_database(processed_directories):
    """ Create chess pieces database from given games """
    for processed_directory in processed_directories:
        print("Processed directory: {}".format(processed_directory))
        game = load_virtual_board(processed_directory)
        board = chess.Board()
        filenames, all_board_images = load_images_to_process(processed_directory)
        all_cropped_images = []
        for img in all_board_images:
            all_cropped_images.extend(crop_image(img))
        pieces_list = read_game_pieces_list(board, game)
        save_cropped_images(all_cropped_images, pieces_list, filenames)


def main():
    """ Create directories for database and save images to appropriate folders """
    create_dirs_for_processed_images()
    create_database(get_all_processed_directories())


if __name__ == "__main__":
    main()
