import os
import cv2
from image_final_crop import return_cropped_image

####################README#########################
# 1.This file should be placed in PATH/chessboard-importer-backend/app/code
# 2.To run: python crop_script.py
# 3.Folders with images must be placed in PATH/chessboard-importer-backend/app/code/data/photos
# 4.Output images(cropped) will appear in PATH/chessboard-importer-backend/app/code/data/croppedd_photos
###################################################

#############CREATING NEEDED FOLDERS###############
open_path = './data/photos'
dirs = os.listdir(open_path)
save_path = './data/cropped_photos'

try:
    os.mkdir(save_path)
except FileExistsError:
    print("Directory exist")

for dir_index, dir in enumerate(dirs):
    try:
        os.mkdir(f"{save_path}/{dir}")
    except FileExistsError:
        print("Directory exist")
####################################################

################CUTTING AND SAVING PHOTOS###########
for dir_index, dir in enumerate(dirs):
    files = os.listdir(f'{open_path}/{dir}')
    for index, file in enumerate(files):
        if file[-3:] == "JPG" or file[-3:] == "jpg":
            file_path = f'{open_path}/{dir}/{file}'
            full_image = cv2.imread(file_path)
            cropped_image = return_cropped_image(full_image)
            cv2.imwrite(f'{save_path}/{dir}/{file}',cropped_image)
####################################################
