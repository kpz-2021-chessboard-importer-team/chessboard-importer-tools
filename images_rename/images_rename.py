'''
1. Adjust first parameter of os.rename() to your camera image
   saves convention(default setup is for iOS)
2. Put that script into game folder
3. Change game folder number to biggest number in games database
4. Change imageNumber to image number of first move in your default image names
5. Change lastImage to image number of last move in your default image names
6. Run script
'''
import os
GAME_FOLDER_NUMBER = 4
GAME_TURN = 1
IMAGE_NUMBER = 1815
LAST_IMAGE = 1867
PLAYER = True
while IMAGE_NUMBER<LAST_IMAGE+1:
    try:
        if PLAYER:
            os.rename(f'IMG_{IMAGE_NUMBER}.JPG', f'{GAME_FOLDER_NUMBER}_{GAME_TURN}_{1}.JPG')
        else:
            os.rename(f'IMG_{IMAGE_NUMBER}.JPG', f'{GAME_FOLDER_NUMBER}_{GAME_TURN}_{2}.JPG')
            GAME_TURN+=1
        PLAYER = not PLAYER
        IMAGE_NUMBER+=1
    except FileNotFoundError:
        IMAGE_NUMBER+=1
